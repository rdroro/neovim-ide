call plug#begin()

" Status bar
Plug 'itchyny/lightline.vim'

" theme
Plug 'arcticicestudio/nord-vim'

" Telescope file finder / picker
" external dependies as fd-find and ripgrep must be installed
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

Plug 'kyazdani42/nvim-web-devicons'

" Emmet for vim
Plug 'mattn/emmet-vim'
call plug#end()

filetype plugin indent on

let g:user_emmet_mode='a'    "enable all function in all mode.

" turn on line numbering
set number relativenumber
set cursorline

" sane text files
set fileformat=unix
set encoding=utf-8
set fileencoding=utf-8

" sane editing
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set autoindent
set smartindent
filetype plugin indent on

" color scheme
colorscheme nord


" lightline
set noshowmode
let g:lightline = { 'colorscheme': 'nord' }

" code folding
set foldmethod=indent
set foldlevel=99

" Telescope map
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>

" Explorer config
let g:netrw_liststyle = 3 
let g:netrw_winsize = 25
let g:netrw_browse_split = 4
map <F2> <cmd>Lexplore<CR>

tnoremap <Esc> <C-\><C-n>

noremap zz <c-w>_ \| <c-w>\|
noremap zo <c-w>=

source ~/.config/nvim/bepo-mapping.vim

" restore place in file from previous session
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Hightlight on yank
" From https://neovim.io/news/2021/07
au TextYankPost * lua vim.highlight.on_yank {higroup="IncSearch", timeout=150, on_visual=true}

" i don't know why … need to be documented
set hidden

