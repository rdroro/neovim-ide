# neovim-ide

My space to store neovim configuration to use it as an IDE using LSP.

Inspired by [https://github.com/MashMB/nvim-ide](https://github.com/MashMB/nvim-ide)

Currently, only for web development (Typescript, HTML, CSS, Dockerfile, JSON and Yaml)

Features :

- Spellcheck in English and french
- Telescope to easily do stuff with this fuzzy finder
- Auto completion via CoQ
- `Nord` theme color scheme
- Web language support via LSP :
    - Typescript
	- HTML
	- CSS
	- Dockerfile
	- JSON
    - Yaml
- BEPO Keyboard Layout and optimization by default (to disable it, remove the `source` from `init.vim`)

## Get Started

To be used as an daily IDE :

    $ docker pull https://registry.gitlab.com/rdroro/neovim-ide 
    # clone the repository (only to easily add features on test some new plugins/configuration)
    $ git clone git@gitlab.com:rdroro/neovim-ide.git
    # Add an alias to your .profile or .bashrc or .zshrc
    # with the config inside container
    $ alias ide-nvim='docker run -it --rm -v `pwd`:/home/node/workspace registry.gitlab.com/rdroro/neovim-ide:latest bash'
    # with the nevoim config mounted into the repository
    $ alias ide-nvim='docker run -it --rm -v /home/rdroro/code/neovim-ide/config/nvim:/root/.config/nvim -v `pwd`:/root/workspace registry.gitlab.com/rdroro/neovim-ide:latest bash'




run `make build-docker-image-for-dev`


## How to build version

* To test new versions/configuration, Dev version for test 

    make docker-build-image-for-dev 

Inside .zshrc

    alias beta-ide-nvim='docker run -it --rm -v /home/rdroro/code/neovim-ide/config/nvim:/root/.config/nvim -v "`pwd`":/root/workspace registry.gitlab.com/rdroro/neovim-ide:dev bash' 

* for stable version

    make docker-increment-and-build

when everything is clean : `make docker-image-push`

## Tips & Tricks 

* enable Coq

    PlugInstall
    COqdeps
    COqnow
enable Spellcheck for fr

    : set spell spelllang=fr
