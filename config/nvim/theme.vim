" color scheme
colorscheme nord

" lightline
set noshowmode
let g:lightline = { 'colorscheme': 'nord' }
