lua << EOF

require'nvim-treesitter.configs'.setup {
    autotag = {
        enable = true
    },
    ensure_installed = {
        "cmake",
        "css",
        "dockerfile",
        "html",
        "javascript",
        "json",
        "scss",
        "toml",
        "tsx",
        "yaml"
    },
}

local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.tsx.filetype_to_parsename = { "javascript", "typescript.tsx" }



EOF
