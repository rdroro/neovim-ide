" Telescope map
nnoremap <leader>ff <cmd>Telescope find_files hidden=true<cr>
nnoremap <leader>fg <cmd>Telescope live_grep hidden=true<cr>
map <leader>fb <cmd>Telescope file_browser hidden=true<CR>
map <leader>ss <cmd>Telescope spell_suggest<CR>
lua << EOF
file_ignore_patterns = {".git/"}
-- file_browser plugin
require("telescope")
    .setup{
        defaults = {
            file_ignore_patterns = {".git/"}
        }
    }
    .load_extension "file_browser"

EOF
