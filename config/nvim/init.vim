source /home/node/.config/nvim/general/settings.vim
source /home/node/.config/nvim/general/bepo-mapping.vim

call plug#begin()

    " git indicator in editor
    Plug 'airblade/vim-gitgutter'

    " Status bar
    Plug 'itchyny/lightline.vim'

    " theme
    Plug 'arcticicestudio/nord-vim'

    " Telescope file finder / picker
    " external dependies as fd-find and ripgrep must be installed
    Plug 'nvim-lua/popup.nvim'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    Plug 'nvim-telescope/telescope-file-browser.nvim'

    Plug 'kyazdani42/nvim-web-devicons'

    " neovim language things
    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
    Plug 'neovim/nvim-lspconfig'
    " Nicer LSP UI
    Plug 'glepnir/lspsaga.nvim'

    " coq for display autocomplete
    Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
    " 9000+ Snippets
    Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}

    " autopairs
    Plug 'windwp/nvim-autopairs'
    Plug 'windwp/nvim-ts-autotag'

    " Buffer explorer - need to explain buffer
    Plug 'jlanzarotta/bufexplorer'

    " Emmet for vim
    Plug 'mattn/emmet-vim'

    " Syntax highlighting for handlebars
    Plug 'mustache/vim-mustache-handlebars'

call plug#end()

source /home/node/.config/nvim/autopairs.vim
source /home/node/.config/nvim/coq.vim
source /home/node/.config/nvim/emmet.vim
source /home/node/.config/nvim/lsp.vim
source /home/node/.config/nvim/telescope.vim
source /home/node/.config/nvim/theme.vim
source /home/node/.config/nvim/treesitter.vim
