filetype plugin indent on
" turn on hybrid line numbering
set number relativenumber
set cursorline

" Keep multiple buffers open
set hidden

" sane text files
set fileformat=unix
set encoding=utf-8
set fileencoding=utf-8

" sane editing
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set autoindent
set smartindent
filetype plugin indent on
" Horizontal splits on bottom
set splitbelow

" Vertical splits on right
set splitright

" code folding
set foldmethod=indent
set foldlevel=99

" remap to escape terminal mode with <esc>
tnoremap <Esc> <C-\><C-n>

" Zoom in/out for windows
noremap zz <c-w>_ \| <c-w>\|
noremap zo <c-w>=

" Hightlight on yank
" From https://neovim.io/news/2021/07
au TextYankPost * lua vim.highlight.on_yank {higroup="IncSearch", timeout=150, on_visual=true}

" 256 colors support
set t_Co=256

" Smooth scroll
set so=999

" Shared clipboard
set clipboard+=unnamedplus

" restore place in file from previous session
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

