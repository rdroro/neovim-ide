container_name = ide-nvim 
workdir = /root/workspace
version = 1.2.0
docker_image = registry.gitlab.com/rdroro/neovim-ide


version:
	@echo $(version)

increment-version:
	@echo not implemented yet
	@echo version need to be change in the Makefile

docker-build-image:
	@echo ---- build docker image: $(docker_image):$(version)
	docker build -t $(docker_image):$(version) -t $(docker_image):latest .

docker-build-image-for-dev:
	@echo ---- build docker image: $(docker_image):dev
	docker build -t $(docker_image):dev .

docker-increment-and-build: increment-version docker-build-image
docker-increment-build-and-publish: increment-version docker-build-image docker-push-image

docker-push-image:
	docker push $(docker_image):$(version)
	docker push $(docker_image):latest

