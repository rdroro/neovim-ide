FROM node:16-bullseye

# Install system dependencies
RUN apt-get update && apt-get install -y \
    apt-utils \
    curl \
    fd-find \
    git \
    locales \
    make \
    python3-pip \
    python3-venv \
    ripgrep \
    xclip \
    autoconf \
    automake \
    g++ \
    libtool \
    tzdata \
    unzip \
    zip

# Update npm and install dependencies for LSP and Typescript development
RUN npm install -g  \
    dockerfile-language-server-nodejs \
    neovim \
    pyright \
    tree-sitter \
    tree-sitter-cli \
    typescript \
    typescript-language-server \
    vscode-langservers-extracted \
    yaml-language-server \
    @lifeart/ember-language-server
# Set image locale.

# That's doesnt work
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV TZ=Europe/Paris

USER node
# Install neovim python package. Who need this ?
RUN pip3 install neovim pynvim

# Install neovim form AppImage
RUN mkdir /home/node/neovim && \
    cd /home/node/neovim && \
    curl -LO https://github.com/neovim/neovim/releases/download/stable/nvim.appimage && \
    chmod u+x nvim.appimage

# Create directory for Neovim spell check dictionaries.
RUN mkdir -p /home/node/.local/share/nvim/site/spell

# Copy Neovim dictionaries.
COPY --chown=node ./spell/ /home/node/.local/share/nvim/site/spell/

COPY --chown=node ./config/nvim /home/node/.config/nvim

# Install vim-plug for vim plugin
RUN whoami && curl -fLo /home/node/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim && ls -l /home/node/.config/nvim/*


# Map `nvim` command to appimage with --app-extract-and-run because appimage failed on the container
# Why ?
RUN echo "alias nvim='/home/node/neovim/nvim.appimage --appimage-extract-and-run'" >> /home/node/.bashrc

# Install plugin listed in init.vim
RUN /home/node/neovim/nvim.appimage --appimage-extract-and-run --headless +PlugInstall +qall

RUN mkdir /home/node/workspace

WORKDIR /home/node/workspace

COPY entrypoint.sh /usr/local/bin/

ENTRYPOINT /usr/local/bin/entrypoint.sh $@

